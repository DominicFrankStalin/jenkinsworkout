package org.jenkins.test_calculator_api;

import org.jenkins.main_calculator_api.CalculatorAPI;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class TestCalculatorAPI {

	CalculatorAPI capi;
	int result;
	
	@BeforeClass
	public void startTest() {
		capi=new CalculatorAPI();	
	}
	
	@BeforeMethod
	public void initResult() {
		result=0;	
	}
	
	@Test(priority=1)
	public void testAdditionWithPositiveValues() {
		result = capi.addition(30, 50);
		Assert.assertEquals(result, 80, "Addition function is working with Positive Numbers");

	}
	
	@Test(priority=2)
	public void testAdditionWithZeroValues() {
		result = capi.addition(0,100);
		Assert.assertEquals(result,100, "Addition function is working, if 1 of the arg is zero");

	}
	
	@AfterMethod
	public void finalResult() {
		
	}
	
	@AfterClass
	public void endTest() {
	
	}
	
	
}
